const convict = require(`convict`);

const config = convict({
    env: {
        doc: `The application environment.`,
        format: [`production`, `development`, `test`],
        default: `development`,
        env: `NODE_ENV`
    },
    port: {
        doc: `The port to bind.`,
        format: `port`,
        default: 3080,
        env: `PORT`,
        arg: `port`
    },
    polly_retries: {
        doc: `Number of retries.`,
        format: Number,
        default: 5,
        env: `POLLY_RETRIES`,
        arg: `polly_retriess`
    },
    mysql: {
        host: {
            doc: `Database host name/IP`,
            format: `*`,
            default: `localhost`,
            env: `DB_HOST`,
            arg: `db-host`
        },
        name: {
            doc: `Database name`,
            format: String,
            default: `demo_db`,
            env: `DB_NAME`,
            arg: `db-name`
        },
        user: {
            doc: `Database username`,
            format: String,
            default: `root`,
            env: `DB_USER`,
            arg: `db-user`
        },
        password: {
            doc: `Database password`,
            format: String,
            default: `root`,
            env: `DB_PASSWORD`,
            arg: `db-password`
        }
    }
});

module.exports = config;